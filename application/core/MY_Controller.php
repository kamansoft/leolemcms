<?php
class MY_Controller extends CI_Controller
{

    //la llave de cada elemento de este 
    //array debe coincidir con el nombre en minuscula
    //de algun controlador
    private $side_menu_items = [
        'user' => [
            'name' => 'users',
            'icon' => "person",

        ],
        'post' => [
            'name' => 'posts',
            'icon' => 'document'
        ],

    ];
    private $is_admin = True;

    protected $admin_view_data = [
        'heading' => [
            'lang' => '',
            'title' => ''
        ],
        'body' => [
            'menu' => '',
            'content' => ''
        ],

    ];

    public function getSideMenuItem($item)
    {
        return $this->side_menu_items[$item];
    }
    public function __construct($is_admin = true)
    {
        parent::__construct();
        //$this->load->library('Session');
        $this->admin_view_data['heading']['lang'] = $this->lang->line('lang_code');
        $this->is_admin = $is_admin;
            
        switch ($this->config->item('enviroment')) {
            case 'dev':

                $this->load->library('console');

                $this->output->enable_profiler(TRUE);
                break;
        }
    }
    private function buildSideMenu($current_class, $current_method = null)
    {
        $side_menu_data = [
            'items' => $this->side_menu_items,
            'current_class' => $current_class,
        ];
        if ($current_method) {
            $side_menu_data['current_method'] = $current_method;
        }
        Console::logName('Sidemenu',$side_menu_data);
        return $this->load->view("admin/side_menu", $side_menu_data, true);
    }

    private function getOutputData()
    {
        //determinar el grupo de templates a usar
        //en caso de no ser admin se asime que se usara el
        //los templates del sitio
        if ($this->is_admin) {
            $output_data['heading'] = $this->load->view('admin/heading', $this->admin_view_data['heading'], true);
            $this->admin_view_data['body']['admin_notifications']=$this->admin->getUnDisplayedAdminNotifications();
            $output_data['body'] = $this->load->view('admin/body', $this->admin_view_data['body'], true);
            $this->admin->setDisplayedAllAdminNotifications();
        } else {
            throw new Exception("aun no se configura nada para no admin");
        }
        return $output_data;
    }
    protected function output($return = False)
    {

        $this->load->view('main', $this->getOutputData(), $return);
        $this->session->unset_userdata('admin_notification');
    }


    protected function setSessionFromPost($session_key_name,$post_filter){
          $data=array();
        Console::log("llamado sesion frompost");
        Console::logName('setSessionFromPost filter  array',$post_filter);
        foreach ($post_filter as $field){

            if ($this->input->post($field)){
                $data[$field]=$this->input->post($field);
            };
        }
        $this->session->set_userdata($session_key_name,$data);
        return $data;
    }
    protected function cleanSesionFromPost($session_key_name){
        $this->session->unset_userdata($session_key_name);
    }
    protected function getAdminOutput($content, $current_class, $current_method = null)
    {
        //enviaremos solo las notificaciones marcadas como no desplegadas


        $admin_output_data = [
            'side_menu' => $this->buildSideMenu($current_class, $current_method),
            'content' => $content
            
        ];
       
        return $this->load->view('admin/main', $admin_output_data, true);
    }
    protected function adminOutput($content, $current_class, $current_method = null)
    {
        // Console::logName('admin Output params', [$content, $current_class, $current_method]);

        $this->admin_view_data['body']['content'] = $this->getAdminOutput($content, $current_class, $current_method);
        $this->output();
    }
    protected function moduleOutput($content, $current_class, $current_method = null)
    {


        //Console::logName('module Output params', [$content, $current_class, $current_method]);



        //Console::logName("elementos notificacion",$notification_arr);
        $this->adminOutput(
            $this->load->view(
                'admin/module_content',
                [
                    'title' => $this->lang->line('Module') . ' ' . $this->lang->line($this->getSideMenuItem($current_class)['name']),
                    'content' => $content
                ],
                true
            ),
            $current_class,
            $current_method
        );
    }
}
