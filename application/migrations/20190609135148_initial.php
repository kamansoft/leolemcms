<?php
class Migration_Initial extends CI_Migration
{
    public function __construct()
    {
        parent::__construct();
        #$this->load->dbforge();

    }

    public function up()
    {

        $default_admin_pass = "admin1234";
        $tabla_category_sql = "CREATE TABLE `category` (
            `id` int(11) NOT NULL,
            `name` varchar(256) NOT NULL,
            `post_id` int(11) NOT NULL,
            `parent_id` int(11) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $tabla_post_sql = "CREATE TABLE `post` (
            `id` int(11) NOT NULL,
            `slug` varchar(255) NOT NULL,
            `title` varchar(255) NOT NULL,
            `content` text NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $tabla_post_categories_sql = "CREATE TABLE `posts_categories` (
            `id` int(11) NOT NULL,
            `post_id` int(11) NOT NULL,
            `category_id` int(11) NOT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

        $tabla_user_sql = "CREATE TABLE `user` (
            `id` int(11) NOT NULL,
            `username` varchar(255) NOT NULL,
            `password` varchar(255) NOT NULL,
            `email` varchar(255) DEFAULT NULL
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
        $usuario_admin_sql = "INSERT INTO `user` (`id`, `username`, `password`, `email`) VALUES
          (1, 'admin', '$default_admin_pass', 'lemyskaman@gmail.com');";

        $keys_category_table_sql = "ALTER TABLE `category` ADD PRIMRY KEY (`id`),
            ADD UNIQUE KEY `name` (`name`),
            ADD KEY `category_post` (`post_id`),
            ADD KEY `parent_category` (`parent_id`);";

        $keys_post_table_sql = "ALTER TABLE `post`
            ADD PRIMARY KEY (`id`),
            ADD UNIQUE KEY `slug` (`slug`);";

        $indices_post_category_table_sql = "ALTER TABLE `posts_categories`
            ADD PRIMARY KEY (`id`),
            ADD KEY `post_on_posts_categories` (`post_id`),
            ADD KEY `category_on_posts_categories` (`category_id`);";

        $indices_user_table_sql = "ALTER TABLE `user`
            ADD PRIMARY KEY (`id`),
            ADD UNIQUE KEY `username` (`username`),
            ADD UNIQUE KEY `email` (`email`);";

        $autoincrement_category_table_sql = "ALTER TABLE `category`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $autoincrement_post_table_sql = "ALTER TABLE `post`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $autoincrement_post_category_table_sql = "ALTER TABLE `posts_categories`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;";
        $autoincrement_user_table_sql = "ALTER TABLE `user`
            MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;";

        $relaciones_category_table_sql = "ALTER TABLE `category`
        ADD CONSTRAINT `category_post` FOREIGN KEY(`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
        ADD CONSTRAINT `parent_category` FOREIGN KEY(`parent_id`) REFERENCES `category` (`id
        `) ON DELETE NO ACTION ON UPDATE NO ACTION;";

        $relaciones_post_categories_table_sql = "ALTER TABLE `posts_categories`
        ADD CONSTRAINT `category_on_posts_categories` FOREIGN KEY(`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
        ADD CONSTRAINT `post_on_posts_categories` FOREIGN KEY(`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
        ";

        $query = $this->db->query($tabla_category_sql);
        $query = $this->db->query($tabla_post_sql);
        $query = $this->db->query($tabla_post_categories_sql);
        $query = $this->db->query($tabla_user_sql);
        $query = $this->db->query($usuario_admin_sql);

        $query = $this->db->query($keys_category_table_sql);
        $query = $this->db->query($keys_post_table_sql);
        $query = $this->db->query($indices_post_category_table_sql);
        $query = $this->db->query($indices_user_table_sql);
        $query = $this->db->query($autoincrement_category_table_sql);
        $query = $this->db->query($autoincrement_post_table_sql);
        $query = $this->db->query($autoincrement_post_category_table_sql);
        $query = $this->db->query($autoincrement_user_table_sql);
        $query = $this->db->query($relaciones_category_table_sql);
        $query = $this->db->query($relaciones_post_categories_table_sql);
    }

    public function down()
    {
        $this->dbforge->drop_table('posts_categories');
        $this->dbforge->drop_table('category');
        $this->dbforge->drop_table('post');
        $this->dbforge->drop_table('user');
    }
}
