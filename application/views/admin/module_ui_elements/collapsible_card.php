<div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
        <div class="card-header-title"> <?php echo $title ?> </div>

        <nav class="card-header-actions">
            <a class="card-header-action" data-toggle="collapse" href="#card1" aria-expanded="false" aria-controls="card1">
                <i data-feather="minus-circle"></i>
            </a>

        </nav>
    </div>
    <div class="card-body collapse show" id="card1">
        <?php echo $content?>
    </div>
</div>