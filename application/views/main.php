<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<!-- Here comes the html document -->
<html lang="<?php echo $lang;?>">
<!-- Here comes the html heading -->
<?php echo $heading?>
<!-- this is where heading ends -->
<!-- Here comes the html Body -->
<?php echo $body?>
<!-- this is where Body ends -->
</html>