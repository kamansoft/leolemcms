<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['lang_code'] = 'es';
$lang['user_login']='Inicio de Sesion';
$lang['user_username']="Nombre de Usuario";
$lang['user_email_address']= "Direccion de Correo";
$lang['user_password']="Contraseña";
$lang['user_remember_me'] = "Recordar";
$lang['sign_in']="Acceder";
$lang['forgot_your_password']="¿ Olvido su Contraseña ?";

$lang['profiler_debug_data']= "Debug de Variables";
$lang['profiler_no_debug']="No hay variables";
$lang['send_data_confirmation']='¿Enviar datos y continuar?';


$lang['Module']='Modulo';
$lang['create']="Crear";
$lang['created']="creado";
$lang['disable']="Desabilitar";
$lang['disabled']="Desabilitado";
$lang['list']='Lista';
$lang['admin']="Administrador";
$lang['editor']="Editor";
$lang['regular']="Regular";
$lang['active']="Activo";
$lang['inactive']="Inactivo";
$lang['accept']="Aceptar";
$lang['cancel']="Cancelar";

$lang['users']='Usuarios';
$lang['users_list']='Lista de Usuarios';
$lang['user']='Usuario';

$lang['user_profile']='Perfil de Usuario';
$lang['user_form']='Formulario de Usuario';



$lang['posts']='Entradas';

?>