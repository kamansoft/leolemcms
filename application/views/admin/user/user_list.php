<div class="col-lg-12">
  <div class="card">
    <div class="card-header d-flex justify-content-between align-items-center">
      <div class="card-header-title"> <?php echo $this->lang->line('users_list') ?> </div>

      <nav class="card-header-actions">
        <a class="card-header-action" data-toggle="collapse" href="#card1" aria-expanded="false" aria-controls="card1">
          <i data-feather="minus-circle"></i>
        </a>

      </nav>
    </div>
    <div class="card-body collapse show" id="card1">
      <h4 class="card-title">Special title treatment</h4>
      <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
      <a href="<?php echo base_url('user/add')?>" class="btn btn-primary">Añadir usuario</a>
    </div>
  </div>
</div>
