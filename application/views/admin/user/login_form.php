<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>


<div class="adminx-container d-flex justify-content-center align-items-center">
    <div class="page-login">
      <div class="text-center">
        <a class="navbar-brand mb-4 h1" href="login.html">
          <img src="<?php echo base_url() ?>static/demo/img/logo.png" class="navbar-brand-image d-inline-block align-top mr-2" alt="">
          Inicio de Sesion
        </a>
      </div>

      <div class="card mb-0">

      <div class="card-footer text-center">
          <?php echo validation_errors(); ?>
          
        </div>
        <div class="card-body">
          <?php echo form_open('user/login'); ?>
          <div class="form-group">
            <label for="exampleDropdownFormEmail1" class="form-label">Usuario</label>
            <input type="" class="form-control" name="username" id="exampleDropdownFormEmail1" placeholder="email@example.com">
          </div>
          <div class="form-group">
            <label for="exampleDropdownFormPassword1" class="form-label">Contraseña</label>
            <input type="password" name="password" class="form-control" id="exampleDropdownFormPassword1" placeholder="Password">
          </div>
          <!--<div class="form-group">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="customCheck1">
              <label class="custom-control-label" for="customCheck1">Remember me</label>
            </div>
          </div>-->
          <button type="submit" class="btn btn-sm btn-block btn-primary">Entrar</button>
          </form>
        </div>
        
      </div>
    </div>
  </div>