<?php
abstract class MY_Model extends CI_Model
{
    protected $query_alias_fields_array = [];
    protected $updater_id = null;
    protected $update_date = null;
    protected $creator_id = null;
    protected $creation_date = null;
    protected $updater;
    protected $creator;

    protected $user_table = 'user';
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function __set($name, $value)
    {
        Console::logName('set name', $name);
        switch ($name) {
            case 'updater_id':

                $this->updater_id = $value;
                $this->setUpdaterUser();
                break;
            case 'creator_id':
                $this->creator_id = $value;
                $this->setCreatorUser();
                break;
            case 'udpate_date':
                $this->setUpdateDate($value);
                break;
            case 'creation_date':
                $this->setCreationDate($value);
                break;
            default:
                $this->$name = $value;
                break;
        }
    }

    abstract public function get_table_name(): string;
    abstract public function get_query_alias_fields_array(): array;
    abstract public function select_query(array $fields = []): CI_DB_query_builder;
    abstract public function retrive($id): CI_DB_result;
    abstract protected function getFull($id = null): CI_DB_result;
    abstract public function create(array $values = []): bool;
    abstract public function update(array $values = []): bool;



    public function get_table_fields()
    {
        return $this->db->list_fields($this->get_table_name);
    }

    public function save()
    {
        $db_object = $this->retrive($this->id)->row();
        if ($db_object) {
            Console::log('save consiguio el objeto');
            Console::log($db_object);
            $this->id=$db_object->id;
            $to_return = $this->update();
            
        } else {
            Console::log('save creara un objeto nuevo');
            $to_return = $this->create(
               
            );
        }
        if ($to_return){
            $this->get();
        }
        return $to_return;
    }

    public function get($id=null)
    {
        $id = ($id) ? $id : ($this->id) ? $this->id : $this->username;
        return $this->merge($this->retrive($id));
    }
    public function merge($dbresult)
    {

        
       $result = $dbresult->result_array();
        if ($result) {
            $row = $result[0];
            Console::log($row);
            foreach ($row as $key => $value) {
                Console::log($key);
                if (array_key_exists($key, $this->get_query_alias_fields_array())) {
                    $this->__set($key, $value);
                }
            }
            return $this;
        }
        return false;
    }

    private function getUser($id)
    {
        $this->db->select('id', 'username', 'name', 'email', 'rol', 'status')
            ->from($this->user_table)
            ->where('id', $id)
            ->limit(1);
            Console::log('get user db hit');
        return $this->db->get();
    }
    public function setUpdaterUser()
    {
        $user = $this->getUser($this->updater_id)->row();
        if ($user) {
            $this->updater = $user;
            return $user;
        } else {
            return false;
        }
    }
    public function setUpdateDate($date)
    {
        $this->update_date = $date;
    }
    public function setCreatorUser()
    {
        $user = $this->getUser($this->creator_id)->row();
        Console::logName('Creator id ',$this->creator_id);
        
        Console::logName('creator user',$user);
        if ($user) {
            
            $this->creator = $user;
            return $user;
        } else {
            return false;
        }
    }
    public function setCreationDate($date)
    {
        $this->creation_date = $date;
    }


    public function get_alias_values_array()
    {
        $all_fields = $this->get_query_alias_fields_array();
        foreach ($all_fields as $field => $table_field_name) {
            if (isset($this->$field)) {
                $all_fields[$field] = $this->$field;
            } else {

                unset($all_fields[$field]);
            }
        }
        return $all_fields;
    }
    public function get_fields_values_array()
    {
        $all_fields = [];
        foreach ($this->get_query_alias_fields_array() as $field => $table_field_name) {

            if (isset($this->$field)) {
                $all_fields[$table_field_name] = $this->$field;
            }
        }
        return $all_fields;
    }
    public function aliases2fields_values($aliases)
    {
        $fields_values = [];

        foreach ($this->get_query_alias_fields_array() as $alias => $field) {
            if (isset($aliases[$alias])) {
                $fields_values[$field] = $aliases[$alias];
            }
        }
        return $fields_values;
    }
    public function getQueryFieldName($query_field)
    {

        return array_search($query_field, $this->get_query_alias_fields_array());
    }
    public function getQueryFieldValue($query_field)
    {
        return get_object_vars($this)[array_search($query_field, $this->get_query_alias_fields_array())];
    }
    public function excludeFields($excludeKeys = null, array $subject = [])
    {
        $to_return = (count($subject) > 0) ? $subject : $this->get_query_alias_fields_array();
        if (is_array($excludeKeys) and count($excludeKeys) > 0) {
            foreach ($excludeKeys as $key) {
                unset($to_return[$key]);
            }
        } elseif (is_string($excludeKeys) and strlen($excludeKeys) > 0) {
            unset($to_return[$excludeKeys]);
        }
        return $to_return;
    }
    public function getQueryFieldsString(array $fields = [])
    {
        $field_aliases = [];
        if (count($fields) > 0) {
            foreach ($fields as $field) {
                array_push($fieldaliases, $key . ' as ' . $this->get_query_alias_fields_array()['key']);
            }
            Console::logName(' aliases', $field_aliases);
            return implode(', ', $field_aliases);
        } else {
            return '*';
        }
    }
}
