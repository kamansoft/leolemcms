<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<div class="adminx-sidebar expand-hover push">
    <ul class="sidebar-nav">
        <?php foreach ($items as $key => $value) : ?>
            <li class="sidebar-nav-item">
                <a href="<?php echo base_url($key)?>" class="sidebar-nav-link <?php echo ($current_class===$key)?"active":"";?>">
                    <span class="sidebar-nav-icon">
                        <i class="oi oi-<?php echo $value['icon'] ?>"></i>
                    </span>
                    <span class="sidebar-nav-name">
                        <?php echo $this->lang->line($value['name'])."" ?>
                        
                    </span>
                    <span class="sidebar-nav-end">

                    </span>
                </a>
            </li>
        <?php endforeach; ?>
    </ul>
</div>