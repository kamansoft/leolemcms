<?php
defined('BASEPATH') or exit('No direct script access allowed');
class User extends MY_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user_model');
		$this->load->helper('url_helper');
		$this->load->library('session');
		$this->load->library('admin');
	}
	public function index($id = null)
	{
		//$this->admin->setSuccessAdminNotiication('redirigido de index');
		$this->admin->addAdminNotification('fuiste redirigido desde index');
		redirect('user/list');
	}

	public function detail($user_id)
	{
		$user = $this->user_model->getById($user_id);
		$user_data = [
			'id' => $user->id,
			'username' => $user->username,
			'name' => $user->name,
			'rol' => $user->status,
			'status'=> $this->lang->line($this->user_model->getStatuses()[$user->status])

		];
		$this->moduleOutput(

			$this->load->view(
				'admin/module_ui_elements/collapsible_card',
				[
					'title' => 'Perfil de Usuario',
					'content' => $this->load->view(
						'admin/user/user_detail',
						$user_data,
						true
					)
				],
				true
			),
			$this->router->fetch_class(),
			$this->router->fetch_method()
		);
	}

	public function me()
	{
		$this->admin->needs_user();
		redirect('user/detail/' . $this->session->user->id);
	}

	public function changePassword()
	{

		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$user = null;
		if (isset($this->session->user->id)) {
			Console::log('usuario desde sesion');
			$user = $this->user_model->get($this->session->user->username);
		} elseif ($this->input->post('username')) {
			Console::log('usuario desde post');
			$user = $this->user_model->get($this->input->post('username'));
		}

		Console::logName('Usuario', $user);

		$form = admin_confirmable_form_open(base_url('user/changePassword'));
		if (!$user) {
			$form .= admin_form_input('Usuario', ['name' => 'username']);
		}


		$form .= admin_form_password('Contraseña Nueva', ['name' => 'password']);
		$form .= admin_form_password('Confirmacion de Contraseña', ['name' => 'passconf']);
		$form .= form_button(['type' => 'submit', 'class' => 'btn btn-sm btn-block btn-primary'], 'Cambiar');
		$form .= form_close();
		$content = [
			'title' => 'Cambio de Constraseña de Usuario',
			'content' => $form
		];

		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('passconf', 'Confirmacion de contraseña', 'required|matches[password]');
		if ($this->form_validation->run() == FALSE) {
			if (validation_errors()) {
				$this->admin->addAdminNotification(validation_errors(), ['style' => 'danger', 'autoHide' => false]);
			}
			$this->moduleOutput(
				$this->load->view(
					'admin/module_ui_elements/collapsible_card',
					$content,
					true
				),
				$this->router->fetch_class(),
				$this->router->fetch_method()
			);
		} else {

			if (
				$this->user_model->changePassword(
					$user->id,
					$this->admin->hashPass(
						$this->input->post('password')
					)
				)
			) {
				$this->admin->addSuccessAdminNotification('Constraseña de usuario Cambiada');
				redirect('user/login');
			} else {
				$this->admin->addDangerAdminNotification('Error Cambiadno constraseña');
				$this->moduleOutput(
					$this->load->view(
						'admin/module_ui_elements/collapsible_card',
						$content,
						true
					),
					$this->router->fetch_class(),
					$this->router->fetch_method()
				);
			}
		}
	}
	public function list()
	{

		$this->admin->needs_user();

		$this->moduleOutput(
			$this->load->view(
				'admin/user/user_list',
				[],
				true
			),
			$this->router->fetch_class(),
			$this->router->fetch_method()

		);
	}

	public function edit($id)
	{
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');
		$data = [
			'form_values' => [],
			'rol_options' => $this->user_model->getRols(),
			'status_options' => $this->user_model->getStatuses(),
			'submit_confirm_message' => '¿ Esta seguro de continuar ?'
		];
		$data['form_values'] = $this->setSessionFromPost('form_values_user_add', $this->user_model->getFields());
		$this->form_validation->set_rules(
			'username',
			'Ususario',
			'required|min_length[3]|is_unique[user.username]',
			array(
				'is_unique'     => 'El %s ya existe.'
			)
		);
		$this->form_validation->set_rules('name', 'required');
		$this->form_validation->set_rules('email', 'Correo Electronico', 'required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('rol', 'in_list["A","E","R"]');
		$this->form_validation->set_rules('status', 'in_list[1,0]');
	}

	public function add()
	{
		$this->admin->needs_user('user/login');
		$this->admin->needs_user_with_rol('A', 'user/list');
		$this->load->helper(array('form', 'url'));
		$this->load->library('form_validation');

		$data = [
			'rol_options' => $this->user_model->getRols(),
			'status_options' => $this->user_model->getStatuses(),
			'submit_confirm_message' => '¿ Esta seguro de continuar ?'
		];
		$data['form_values'] = $this->setSessionFromPost('form_values_user_add', $this->user_model->getFields());
		//valores por defecto de formulario
		if (!isset($data['form_values'])) {
			$data['form_values'] = [
				'rol' => 'R',
				'statis' => 'inact'
			];
		}

		Console::logName('fields', $this->user_model->getFields());


		Console::logName('form data', $data);
		$this->form_validation->set_rules(
			'username',
			'Ususario',
			'required|min_length[3]|is_unique[user.username]',
			array(
				'is_unique'     => 'El %s ya existe.'
			)
		);
		$this->form_validation->set_rules('name', 'required');
		$this->form_validation->set_rules('password', 'Contraseña', 'required');
		$this->form_validation->set_rules('passconf', 'Confirmacion de contraseña', 'required|matches[password]');
		$this->form_validation->set_rules('email', 'Correo Electronico', 'required|valid_email|is_unique[user.email]');
		$this->form_validation->set_rules('rol', 'in_list["A","E","R"]');
		$this->form_validation->set_rules('status', 'in_list[1,0]');

		if ($this->form_validation->run() == FALSE) {
			$form = $this->load->view('admin/user/user_form', $data, true);
			Console::logName('validationerror', validation_errors());
			if (validation_errors()) {
				$this->admin->addAdminNotification(validation_errors(), ['style' => 'danger', 'autoHide' => false]);
			}
			$this->moduleOutput($form, $this->router->fetch_class(), $this->router->fetch_method());
		} else {
			$db_add = $this->user_model->add($data['form_values']);
			if ($db_add) {
				$this->admin->addSuccessAdminNotification('Usuario <b>' . $db_add['username'] . '</b> Exitosamente Agregado');
				$this->cleanSesionFromPost('form_values_user_add');
				redirect('user/list');
			} else {

				$this->admin->addDangerAdminNotification('Usuario no añadido');
				redirect('user/add');
			}


			//$this->admin->setSuccessAdminNotiication('Usuario Exitosamente Agregado');

		}
	}
	public function _verify($password, $username)
	{
		return $this->admin->verifyUser($password, $username);
	}


	public function logout()
	{

		$this->session->sess_destroy();
		redirect('user/login');
	}
	public function login()
	{
		if (isset($this->session->user)) {
			redirect('user');
		} else {
			$this->load->helper('form');

			$this->load->library('form_validation');

			$this->form_validation->set_rules('username', 'Usuario', 'required');
			$this->form_validation->set_rules(
				'password',
				'Contraseña',
				'required|callback__verify[' . $this->input->post('username') . ']'

			);
			$this->form_validation->set_message('_verify', 'Usuario / Contraseña no valido');
			if ($this->form_validation->run() === FALSE) {
				$form_data = [];
				$this->admin_view_data['body']['content'] = $this->load->view('admin/user/login_form', $form_data, true);
				$this->output();
			} else {
				$this->admin->setAdminUserSession($this->input->post('username'));

				$this->admin->addSuccessAdminNotification('Bienvenido <b>' . $this->session->user->name . '</b> ud se ha logeado correctamente');
				redirect('user');
			}
		}
	}
}
