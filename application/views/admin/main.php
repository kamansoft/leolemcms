<div class="adminx-container">
    <nav class="navbar navbar-expand justify-content-between fixed-top">
        <a class="navbar-brand mb-0 h1 d-none d-md-block" href="index.html">
            <img src="<?php echo base_url() ?>static/demo/img/logo.png" class="navbar-brand-image d-inline-block align-top mr-2" alt="">
            LeoLemCMS
        </a>



        <div class="d-flex flex-1 d-block d-md-none">
            <a href="#" class="sidebar-toggle ml-3">
                <i data-feather="menu"></i>
            </a>
        </div>

        <ul class="navbar-nav d-flex justify-content-end mr-2">

            <?php if (isset($this->session->user->name)) : ?>

                <li class="nav-item dropdown">
                    <a class="nav-link avatar-with-name" id="navbarDropdownMenuLink" data-toggle="dropdown" href="#">
                        <?php echo (isset($this->session->user->name)) ? $this->session->user->name : '-' ?>

                        <img src="<?php echo base_url() ?>static/dist/media/man-user.png" class="d-inline-block align-top" alt="">

                    </a>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#"><?php echo (isset($this->session->user->username)) ? $this->session->user->username : '-' ?></a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item text-danger" href="<?php echo base_url('user/logout') ?>">Salir</a>
                    </div>
                </li>
            <?php endif ?>
        </ul>
    </nav>

    <!-- expand-hover push -->
    <!-- Sidebar -->
    <?php echo $side_menu ?>
    <!-- Sidebar End -->

    <!-- adminx-content-aside -->
    <div class="adminx-content">
        <!-- <div class="adminx-aside">

        </div> -->

        <div class="adminx-main-content">
            <div class="container-fluid">
                <?php echo $content ?>


            </div>

        </div>
    </div>

</div>