<div class="col-lg-12">
    <div class="card">
        <div class="card-header d-flex justify-content-between align-items-center">
            <div class="card-header-title"> <?php echo $this->lang->line('user_form') ?> </div>

            <nav class="card-header-actions">
                <a class="card-header-action" data-toggle="collapse" href="#card1" aria-expanded="false" aria-controls="card1">
                    <i data-feather="minus-circle"></i>
                </a>

            </nav>
        </div>
        <div class="card-body collapse show" id="card1">
            <?php echo admin_confirmable_form_open('user/add', ['id' => 'user_form']); ?>
        
            <div class="form-group">
                <label for="id" class="form-label">Id Usuario</label>
                <input disabled="disabled" type="" class="form-control" name="id" id="id" value="<?php echo (isset($form_values['id'])) ? $form_values['id'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="username" class="form-label">Usuario</label>
                <input type="text" class="form-control" name="username" id="username" placeholder="usuario123" value="<?php echo (isset($form_values['username'])) ? $form_values['username'] : '' ?>">
            </div>
            <div class="form-group">
                <label for="name" class="form-label">Nombre Completo</label>
                <input type="text" class="form-control" name="name" id="name" placeholder="John Doe" value="<?php echo (isset($form_values['name'])) ? $form_values['name'] : '' ?>">
            </div>

            <?php if($this->router->fetch_method()==='user/add'):?>
            <div class="form-group">
                <label for="password" class="form-label">Contraseña</label>
                <input type="password" name="password" class="form-control" id="password" placeholder="Contraseña">
            </div>
            <div class="form-group">
                <label for="passconf" class="form-label">Confirmacion de Contraseña</label>
                <input type="password" name="passconf" class="form-control" id="passconf" placeholder="Confirmacion de contraseña">
            </div>
            <?php endif?>
            
            <div class="form-group">
                <label for="email" class="form-label">Correo Electronico</label>
                <input type="" class="form-control" name="email" id="email" placeholder="email@example.com" value="<?php echo (isset($form_values['email'])) ? $form_values['email'] : '' ?>">
            </div>
            <div class="form-group">
                <label class="form-label" for="rol">Seleccione Rol de Usuario</label>
                <select class="form-control" id="rol" name="rol">
                    <?php foreach ($rol_options as $key => $value) : ?>
                        <option value="<?php echo $key ?>" <?php echo (isset($form_values['rol']) and $form_values['rol'] === $key) ? 'selected' : '' ?>> <?php echo $this->lang->line($value) ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <div class="form-group">
                <label class="form-label" for="status">Estado de Usuario</label>
                <select class="form-control" id="status" name="status">
                    <?php foreach ($status_options as $key => $value) : ?>
                        <option value="<?php echo $key ?>" <?php echo (isset($form_values['status']) and $form_values['status'] === $key) ? 'selected' : '' ?>> <?php echo $this->lang->line($value) ?></option>
                    <?php endforeach ?>
                </select>
            </div>

            <button class="btn btn-sm btn-block btn-primary">Guardar</button>
            </form>

        </div>
    </div>
</div>