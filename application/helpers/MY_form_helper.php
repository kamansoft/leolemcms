<?php

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		lemyskaman
 */

// ------------------------------------------------------------------------




if (!function_exists('admin_confirmable_form_open')) {
	/**
	 * Form Declaration
	 *
	 * Creates the opening portion of the form.
	 *
	 * @param	string	the URI segments of the form destination
	 * @param	array	a key/value pair of attributes
	 * @param	array	a key/value pair hidden data
	 * @return	string
	 */
	function admin_confirmable_form_open($action = '', $attributes = array(), $hidden = array())
	{
		$CI = &get_instance();

		// If no action is provided then set to the current url
		if (!$action) {
			$action = $CI->config->site_url($CI->uri->uri_string());
		}
		// If an action is not a full URL then turn it into one
		elseif (strpos($action, '://') === FALSE) {
			$action = $CI->config->site_url($action);
		}
		$confirmation_script = '';
		if (isset($attributes['id'])) {

			$confirmation_script = '
		<script>
		document.addEventListener("DOMContentLoaded", function() {
		  
			const ' . $attributes['id'] . ' = document.getElementById("' . $attributes['id'] . '");
			' . $attributes['id'] . '.addEventListener("submit", e => {
			  e.preventDefault();
			  var options = {
				autoHide: false,
				playSound: true,
				style: \'warning\',
			  };
			  var message = "' . $CI->lang->line('send_data_confirmation') . '";
			  var accept_buton_html = \'<button type="button" onclick="' . $attributes['id'] . '.submit()" class="btn btn-sm btn-success">' . $CI->lang->line('accept') . '</button>\';
			  var cancel_buton_html = \'<button type="button" class=" btn btn-sm btn-danger notification-close-btn"  >' . $CI->lang->line('cancel') . '</button>\';
			  var content_html = \'<div class="row"> <div class="col-lg-10" >\' + message + \'</div> <div class="col-lg-2 btn-group btn-group-sm" role="group" >\' + accept_buton_html + \' \' + cancel_buton_html + \'</div> </div>\'
			  notifications.fire(content_html, options);
			});
	
		});
	  </script>
		
		';
		}
		$attributes = _attributes_to_string($attributes);

		if (stripos($attributes, 'method=') === FALSE) {
			$attributes .= ' method="post"';
		}

		if (stripos($attributes, 'accept-charset=') === FALSE) {
			$attributes .= ' accept-charset="' . strtolower(config_item('charset')) . '"';
		}

		$form = '<form action="' . $action . '"' . $attributes . ">\n$confirmation_script";
		if (is_array($hidden)) {
			foreach ($hidden as $name => $value) {
				$form .= '<input type="hidden" name="' . $name . '" value="' . html_escape($value) . '" />' . "\n";
			}
		}

		// Add CSRF field if enabled, but leave it out for GET requests and requests to external websites
		if ($CI->config->item('csrf_protection') === TRUE && strpos($action, $CI->config->base_url()) !== FALSE && !stripos($form, 'method="get"')) {
			// Prepend/append random-length "white noise" around the CSRF
			// token input, as a form of protection against BREACH attacks
			if (FALSE !== ($noise = $CI->security->get_random_bytes(1))) {
				list(, $noise) = unpack('c', $noise);
			} else {
				$noise = mt_rand(-128, 127);
			}

			// Prepend if $noise has a negative value, append if positive, do nothing for zero
			$prepend = $append = '';
			if ($noise < 0) {
				$prepend = str_repeat(" ", abs($noise));
			} elseif ($noise > 0) {
				$append  = str_repeat(" ", $noise);
			}

			$form .= sprintf(
				'%s<input type="hidden" name="%s" value="%s" />%s%s',
				$prepend,
				$CI->security->get_csrf_token_name(),
				$CI->security->get_csrf_hash(),
				$append,
				"\n"
			);
		}

		return $form;
	}
}
if (!function_exists('generateRandomString')) {
	function generateRandomString($length = 10)
	{
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}

// ------------------------------------------------------------------------
if (!function_exists('admin_form_input_label')) {
	/**
	 * Text Input Field label
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	function admin_form_input_label($data = '', $value = '', $extra = '')
	{
		$defaults = array(
			'for' => is_array($data) ? '' : $data,
			'class' => ' form-label'
		);

		return '<label ' . _parse_form_attributes($data, $defaults) . _attributes_to_string($extra) . " />$value</label>\n";
	}
}
// ------------------------------------------------------------------------
if (!function_exists('admin_form_input')) {
	/**
	 * Admin Text Input Field
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @param	string
	 * @return	string
	 */
	function admin_form_input(string $label='',$data = '', $value = '', $extra = '')
	{

		if (!isset($data['id'])){
			$data['id']=generateRandomString();
		}
		$defaults = array(
			'id' => $data['id'],
			'type' => 'text',
			'name' => is_array($data) ? '' : $data,
			'value' => $value,
			'class ' => ' form-control'
		);
		$label=admin_form_input_label($data['id'],$label);
		return $label.'<input ' . _parse_form_attributes($data, $defaults) . _attributes_to_string($extra) . " />\n";
	}
}

// ------------------------------------------------------------------------

if (!function_exists('admin_form_password')) {
	/**
	 * Password Field
	 *
	 * Identical to the input function but adds the "password" type
	 *
	 * @param	mixed
	 * @param	string
	 * @param	mixed
	 * @return	string
	 */
	function admin_form_password(string $label='',$data = '', $value = '', $extra = '')
	{
		is_array($data) or $data = array('name' => $data);
		$data['type'] = 'password';
		return admin_form_input($label,$data, $value, $extra);
	}
}
