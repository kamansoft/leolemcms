<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin
{

    private $system_admin_id = 1;
    private $hash_algorithm = 'sha256';
    protected $CI;


    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('Session');
        $this->CI->load->library('user_agent');
        $this->CI->load->helper('url_helper');
        $this->CI->load->model('user_model');
        
        
        
        if (!$this->CI->session->admin_notifications or count($this->CI->session->admin_notifications) < 1) {
            $this->CI->session->admin_notifications = [];
        }
    }









 public function get_current_user(){
     if ($this->isLogged()){
         return $this->CI->session->user;
     }else{
         $user = new stdClass;
         $user->id=$this->system_admin_id;
         return $user;
     }
 }
   
    
    public function isLogged()
    {
        if (isset($this->CI->session->user)) {
            return true;
        } else {
            return false;
        }
    }
    public function addAdminNotification($message, array $options = [])
    {

        /*
        $this->CI->session->admin_notification = [
            'message' => $message,
            'displayed' => false,
            'options' => [
                'autoHide' => (isset($options['autoHide'])) ? $options['autoHide'] : 'true',
                'playSound' => (isset($options['playSound'])) ? $options['playSound']  : 'true',
                'duration' => (isset($options['duration'])) ? $options['duration'] : '5000',
                'style' => (isset($options['style'])) ? $options['style'] : 'default',
                'position' => (isset($options['position'])) ? $options['position'] : 'top',
            ]
        ];*/



        $all_notifications = $this->CI->session->admin_notifications;

        array_push($all_notifications, [
            'message' => $message,
            'displayed' => false,
            'options' => [
                'autoHide' => (isset($options['autoHide'])) ? ($options['autoHide']===false)?'false':'true' : 'true',
                'playSound' => (isset($options['playSound'])) ? ($options['autoHide']===false)?'false':'true'  : 'true',
                'duration' => (isset($options['duration'])) ? $options['duration'] : '5000',
                'style' => (isset($options['style'])) ? $options['style'] : 'default',
                'position' => (isset($options['position'])) ? $options['position'] : 'top',
            ]
        ]);
        $this->CI->session->admin_notifications = $all_notifications;
    }
    public function getUnDisplayedAdminNotifications()
    {
        $undisplayed = [];

        foreach ($this->CI->session->admin_notifications as $key => $value) {
            if ($value['displayed'] === false) {
                $undisplayed[$key] = $value;
            }
        }
        Console::logName('undisplayed notifications', $this->CI->session->admin_notifications);
        return $undisplayed;
    }
    public function setDisplayedAllAdminNotifications()
    {
        Console::log('setting displayed all notifications');
        $all_notifications = $this->CI->session->admin_notifications;
        foreach ($all_notifications as $key => $value) {
            $all_notifications[$key]['displayed'] = True;
        }
        $this->CI->session->admin_notifications = $all_notifications;
    }
    public function addSuccessAdminNotification($message)
    {

        $this->addAdminNotification($message, [
            'style' => 'success',

        ]);
    }
    public function addDangerAdminNotification($message)
    {

        $this->addAdminNotification($message, [
            'style' => 'danger',

        ]);
    }


    public function needs_user(String $url = null)
    {

        if (!$this->isLogged()) {
            $this->addDangerAdminNotification('Es necesario iniciar sesion');

            if ($this->CI->agent->is_referral()) {
                $redirect_url = $this->agent->referrer();
            } else {
                $redirect_url = 'user/login';
            }

            redirect($redirect_url);
        }
    }

    public function needs_user_with_rol($rol, $url)
    {

        $db_user = $this->CI->user_model->getById($this->CI->session->user->id);
        Console::log("check user with rol ");
        Console::logName('rol needed',$rol);
        Console::logName('duser rol', $db_user->rol);
        if (!isset($db_user) and $db_user->rol!==$rol) {
            Console::log('redirigir');
            $this->addDangerAdminNotification('Accion no permitida, es necesario usuario con rol ' . $this->CI->lang->line($this->CI->user_model->getRols()[$rol]) . '');
            if ($url) {
                $redirect_url = $url;
            } else {
                if ($this->agent->is_referral()) {
                    $redirect_url = $this->agent->referrer();
                } else {
                    $redirect_url = 'user/login';
                }
            }

            //redirect($redirect_url);
        }
    }

    public function referralRedirect(string $url=null){
        
        $redirect ='';
        if ($this->agent->is_referral()) {
            $redirect_url = $this->agent->referrer();
        } else if ($url) {
         $redirect_url = $url;
        }
        redirect($redirect_url);
    }

    public function adminRedirect(string $url=null){
        $redirect_url=($url)?$url:'admin';
        $this->referralRedirect($redirect_url);
    }

    public function setAdminUserSession($username)
    {

        $user_data = $this->CI->user_model->get($username)->row();

        $this->CI->session->user = $user_data;
    }
    public function hashPass($password)
    {
        return hash($this->hash_algorithm, $password);
    }

    public function  verifyUser($password=null, $username=null)
    {
        //ar_dump([$password,$username]);
        if ($username==null and $password==null ){
            $username=$this->CI->user_model->username;
            $password=$this->CI->user_model->password;
        }
        $this->CI->db->where('username',$username);
        $user = $this->CI->db->get($this->CI->user_model->get_table_name())->row();
        Console::logName('user for admin',$user);

        if (isset($user->password) and  $user->password== $this->hashPass($password) ) {
            return true;
        } else {
            return false;
        }
    }
}
