-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 05-07-2019 a las 09:04:58
-- Versión del servidor: 5.7.26-0ubuntu0.18.04.1
-- Versión de PHP: 7.2.19-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `leolemcms`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `post_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `category`
--
DELIMITER $$
CREATE TRIGGER `category_insert_check` BEFORE INSERT ON `category` FOR EACH ROW begin 
	set NEW.creation_date= CURRENT_TIMESTAMP() ;
    set New.updater_id = new.creator_id;
    set New.update_date = new.creation_date;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `category_update_check` BEFORE UPDATE ON `category` FOR EACH ROW begin 
	set NEW.creation_date= old.creation_date;
    set new.creator_id = old.creator_id;
    
    set New.update_date = CURRENT_TIMESTAMP() ;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extra_content`
--

CREATE TABLE `extra_content` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `post_id` int(11) NOT NULL,
  `extra_content_field_id` int(11) NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `extra_content_field`
--

CREATE TABLE `extra_content_field` (
  `id` int(11) NOT NULL,
  `value` varchar(255) NOT NULL,
  `posts_categories_id` int(11) NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Disparadores `extra_content_field`
--
DELIMITER $$
CREATE TRIGGER `extra_content_field_BEFORE_INSERT` BEFORE INSERT ON `extra_content_field` FOR EACH ROW BEGIN

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `post`
--
DELIMITER $$
CREATE TRIGGER `post_insert_check` BEFORE INSERT ON `post` FOR EACH ROW begin 
	set NEW.creation_date= CURRENT_TIMESTAMP() ;
    set New.updater_id = new.creator_id;
    set New.update_date = new.creation_date;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `post_update_check` BEFORE UPDATE ON `post` FOR EACH ROW begin 
	set NEW.creation_date= old.creation_date;
    set new.creator_id = old.creator_id;
    
    set New.update_date = CURRENT_TIMESTAMP() ;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `posts_categories`
--

CREATE TABLE `posts_categories` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `creation_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `posts_categories`
--
DELIMITER $$
CREATE TRIGGER `post_categories_insert_check` BEFORE INSERT ON `posts_categories` FOR EACH ROW begin 
	set NEW.creation_date= CURRENT_TIMESTAMP() ;
    set New.updater_id = new.creator_id;
    set New.update_date = new.creation_date;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `post_categories_update_check` BEFORE UPDATE ON `posts_categories` FOR EACH ROW begin 
	set NEW.creation_date= old.creation_date;
    set new.creator_id = old.creator_id;
    
    set New.update_date = CURRENT_TIMESTAMP() ;
end
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `rol` set('A','E','R') NOT NULL DEFAULT 'R',
  `status` set('act','inact') NOT NULL DEFAULT 'inact',
  `creation_date` timestamp NULL DEFAULT NULL,
  `creator_id` int(11) NOT NULL,
  `update_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updater_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Disparadores `user`
--
DELIMITER $$
CREATE TRIGGER `user_insert_check` BEFORE INSERT ON `user` FOR EACH ROW begin 
	set NEW.creation_date= CURRENT_TIMESTAMP() ;
    set New.updater_id = new.creator_id;
    set New.update_date = new.creation_date;
end
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `user_update_check` BEFORE UPDATE ON `user` FOR EACH ROW begin 
	set NEW.creation_date= old.creation_date;
    set new.creator_id = old.creator_id;
    
    set New.update_date = CURRENT_TIMESTAMP() ;
end
$$
DELIMITER ;

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`),
  ADD KEY `category_post` (`post_id`),
  ADD KEY `parent_category` (`parent_id`),
  ADD KEY `category_creator` (`creator_id`),
  ADD KEY `category_updater` (`updater_id`);

--
-- Indices de la tabla `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD PRIMARY KEY (`id`,`ip_address`),
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indices de la tabla `extra_content`
--
ALTER TABLE `extra_content`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_extra_content_value_per_posts_adn_field` (`extra_content_field_id`,`post_id`),
  ADD KEY `fk_extra_content_post1_idx` (`post_id`),
  ADD KEY `fk_extra_content_extra_content_field1_idx` (`extra_content_field_id`),
  ADD KEY `extra_content_creator` (`creator_id`),
  ADD KEY `extra_content_updater` (`updater_id`);

--
-- Indices de la tabla `extra_content_field`
--
ALTER TABLE `extra_content_field`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_extra_content_field_per_category` (`value`,`posts_categories_id`),
  ADD KEY `fk_extra_content_field_posts_categories1_idx` (`posts_categories_id`),
  ADD KEY `extra_content_field_creator` (`creator_id`),
  ADD KEY `extra_content_field_updater` (`updater_id`);

--
-- Indices de la tabla `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `post_creator` (`creator_id`),
  ADD KEY `post_updater` (`updater_id`);

--
-- Indices de la tabla `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_on_posts_categories` (`post_id`),
  ADD KEY `category_on_posts_categories` (`category_id`),
  ADD KEY `posts_categories_creator` (`creator_id`),
  ADD KEY `posts_categories_updater` (`updater_id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `user_creator` (`creator_id`),
  ADD KEY `user_updater` (`updater_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `extra_content`
--
ALTER TABLE `extra_content`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `posts_categories`
--
ALTER TABLE `posts_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=400008;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `category`
--
ALTER TABLE `category`
  ADD CONSTRAINT `category_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `category_post` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `category_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `parent_category` FOREIGN KEY (`parent_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `extra_content`
--
ALTER TABLE `extra_content`
  ADD CONSTRAINT `extra_content_field` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_extra_content_extra_content_field1` FOREIGN KEY (`extra_content_field_id`) REFERENCES `extra_content_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_extra_content_post1` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `extra_content_field`
--
ALTER TABLE `extra_content_field`
  ADD CONSTRAINT `extra_content_field_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `extra_content_field_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_extra_content_field_posts_categories1` FOREIGN KEY (`posts_categories_id`) REFERENCES `posts_categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `post`
--
ALTER TABLE `post`
  ADD CONSTRAINT `post_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `posts_categories`
--
ALTER TABLE `posts_categories`
  ADD CONSTRAINT `category_on_posts_categories` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `post_on_posts_categories` FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `posts_categories_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `posts_categories_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_creator` FOREIGN KEY (`creator_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `user_updater` FOREIGN KEY (`updater_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
