<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
  <head>
    <title><?php echo $title ;?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>/static/dist/css/adminx.css" media="screen" />
  </head>

