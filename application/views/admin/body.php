<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<body>
  <script>
    window.base_url = "<?php echo base_url() ?>"
  </script>
  <?php echo $content ?>
  <!-- If you prefer jQuery these are the required scripts -->
  <script src="<?php echo base_url() ?>static/dist/js/jquery-3.2.1.min.js"></script>
  <script src="<?php echo base_url() ?>static/dist/js/popper.min.js"></script>
  <script src="<?php echo base_url() ?>static/dist/js/bootstrap.min.js"></script>
  <script src="<?php echo base_url() ?>static/dist/js/vendor.js"></script>
  <script src="<?php echo base_url() ?>static/dist/js/adminx.js"></script>
  <script>
    const notifications = new window.notifications();
  </script>
  <script>
    document.addEventListener("DOMContentLoaded", function() {




      <?php if (isset($admin_notifications)) : ?>
        <?php foreach ($admin_notifications as $key => $admin_notification) : ?>
          var notification_message = `<?php echo $admin_notification['message'] ?>`;
          var notification_options = {
            autoHide: <?php echo $admin_notification['options']['autoHide'] ?>,
            playSound: <?php echo $admin_notification['options']['playSound'] ?>,
            duration: <?php echo $admin_notification['options']['duration'] ?>,
            style: '<?php echo $admin_notification['options']['style'] ?>',
            position: '<?php echo $admin_notification['options']['position'] ?>',
          };
          notifications.fire(notification_message, notification_options);
        <?php endforeach ?>
      <?php endif ?>



    });
  </script>

  <!-- If you prefer vanilla JS these are the only required scripts -->
  <!-- script src="../dist/js/vendor.js"></script>
    <script src="../dist/js/adminx.vanilla.js"></script-->

</body>