<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User_model extends My_Model
{


    private $rols = [
        'A' => 'admin',
        'E' => 'editor',
        'R' => 'regular'
    ];
    private $statuses = [
        'act' => 'active',
        'inact' => 'inactive'
    ];
    


    public $id;
    public $username;
    public $name;
    public $password;
    public $email;
    public $rol;
    public $status;


    public function __construct()
    {
        parent::__construct();
        //$this->load->library('Admin');
    }
    public function get_table_name():string{
        return 'user';
    }
    public function get_query_alias_fields_array(): array{

        return [
            'id' => 'user.id',
            'username' => 'user.username',
            'name' => 'user.name',
            'password' => 'user.password',
            'email' => 'user.email',
            'rol' => 'user.rol',
            'status' => 'user.status',
            'creation_date' => 'user.creation_date',
            'creator_id' => 'user.creator_id',
            'update_date' => 'user.update_date',
            'updater_id' => 'user.updater_id'
        ];
    }


    public function select_query(array $fields = []): CI_DB_query_builder
    {
        $this->db->select((count($fields) > 0) ? $fields : $this->excludeFields('password'));
        return $this->db->from($this->get_table_name());
    }
    public function retrive($id): CI_DB_result
    {
        
        $this->select_query()
            ->where('id', $id)
            ->or_where('username', $id)
            ->limit(1);
        Console::log('retrive db hit' );
        return $this->db->get();
    }
    public function getFull($id = null): CI_DB_result
    {
        $this->select_query($this->get_query_alias_fields_array())
            ->where('id', ($id) ? $id : $this->id);
            Console::log('get full db hit ' );
        return $this->db->get();
    }




    public function create(array $values = []): bool
    {
        if (count($values) > 0) {
            if (isset($values['password'])) {
                unset($values['password']);
            }
            $values = $this->aliases2fields_values($values);
        } else {
            $values = $this->get_fields_values_array();
        }
        $this->db->set('updater_id',$this->admin->get_current_user()->id);
        $this->db->set('creator_id',$this->admin->get_current_user()->id);


        if ($this->db->insert(
            $this->get_table_name(),
            $values,
            true
        )) {
            $this->id = $this->db->insert_id();
            return true;
        } else {
            return false;
        }
    }
    public function update(array $values = []): bool
    {
        $id = null;

        if (count($values) > 0) {
            $values['updater_id']=$this->Admin->session->user->id;
            if (isset($values['id'])) {
                $id = $values['id'];
                unset($values['id']);
            }

            $values = $this->aliases2fields_values(
                $this->excludeFields(
                    [
                        'password',
                        'id',
                        'creator_id',
                        'creation_date',
                        'update_date',
                        'updater_id'
                    ],
                    $values
                )
            );
        } else {
            $id = $this->id;
            $values = $this->aliases2fields_values(
                $this->excludeFields(
                    [
                        'password',
                        'id',
                        'creator_id',
                        'creation_date',
                        'update_date',
                        'updater_id'
                    ],
                    $this->get_alias_values_array()
                )
            );
        }
 
        Console::logName('values to update',$values);
        $this->db->set('updater_id',$this->admin->get_current_user()->id);
        return $this->db->update(
            $this->get_table_name(),
            $values,
            ['id' => $id],
            1
        );
    }



    public function changePassword($newPass, $id = null)
    {
        if (!$id) {
            $id = $this->id;
        }
        $newPass = $this->admin->hashPass($newPass);

        return $this->db->update(
            $this->get_table_name(),
            ['password' => $newPass],
            ['id' => $id],
            1
        );
    }
}
